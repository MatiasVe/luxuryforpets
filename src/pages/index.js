import Header from "../components/Header";
import BannerIndex from "../components/BannerIndex";
import Produtos from "../components/Produtos";
import PecaJa from "../components/PecaJa";
import Footer from "../components/Footer";

function Index(props) {
  return (
    <>
      <Header/>
      <BannerIndex/>
      <Produtos/>
      <PecaJa/>
      <Footer/>
    </>
  )
}

export default Index;
import styles from "./Sobre.module.scss";
import Slider from "react-slick";
import { FiArrowRightCircle, FiArrowLeftCircle } from "react-icons/fi";


function Sobre(props) {

    return (
        <>
            <section className={styles.sobre}>
                <div className="container">
                    <div className="row">
                        <div className="coluna">
                            <article>
                                <h1 className={styles.texto}>Rony Peterson Luxury For Pets</h1>
                                <p className={styles.texto}>
                                    O conceito que estava faltando no mercado de embelezamento pet.
                                </p>
                                <p className={styles.texto}>
                                    A Luxury For Pets é a marca que veio para revolucionar o 
                                    mercado de embelezamento pet. Trazendo tudo o que há de 
                                    mais novo no mercado, nós oferecemos peças inovadoras, 
                                    e o melhor: limitadas. Temos a consciência de que a inovação 
                                    e criatividade é o que nos motiva, e nos destaca. 
                                </p>
                            </article>
                        </div>  
                    </div>
                    <div className="row">
                        <div className={styles.coluna}>
                            <img src="/assets/img/107828.png" alt="Soluções"/>
                            <p className={styles.info}>
                               Qualidade comprovada.
                            </p>
                            <p className={styles.info}>
                                Com a experiência e qualidade do salom
                                juntamos todas as opiniões de nossos clientes
                                para ter um produto único e personalizado,
                                coma cara de cada um de nossos clientes.
                            </p>
                        </div>
                        <div className={styles.coluna}>
                            <img src="/assets/img/pets.png" alt="confidencialidade"/>
                            <p className={styles.info}>
                               Possuimos proficionais qualificados 
                               com varios cursos, possuindo assim 
                               experiência e um toque a mais na escolha do melhor 
                               adereço para seu pet.
                            </p>
                        </div>
                        <div className={styles.coluna}>
                            <img src="/assets/img/change.png" alt="Soluções"/>
                            <p className={styles.info}>
                                Em constante mudança, sempre ouvindo 
                                feedback de clientes para proporcionar
                                o melhor produto com a melhor qualidade.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Sobre;
import styles from "./Titulo.module.scss";

function Titulo(props) {

    return (
        <>
            <section className={styles.title}>
                <div className={styles.container}>
                    <div className="row">
                        <div className="coluna">
                            <header>
                                <h1 className={styles.header}>CONHEÇA NOSSOS SERVIÇOS</h1>
                            </header>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Titulo;
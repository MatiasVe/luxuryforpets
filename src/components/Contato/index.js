import styles from "./Contato.module.scss";
import { FaWhatsapp, FaPhoneAlt, FaInstagram, FaFacebookF } from "react-icons/fa";

function Contato(props) {

    return (
        <>
            <section className={styles.contato}>
                <div className="container">
                    <div className="row">
                        <div className="coluna">
                            <h1>Tire suas dúvidas fale com a gente</h1>
                            {/* <p className={styles.fone}><FaPhoneAlt className={styles.icon}/>85 9.9999-9999</p> */}
                        </div>
                        <div className="coluna">
                            <h1>Veja nossos posts</h1>
                            {/* <p className={styles.fone}><FaPhoneAlt className={styles.icon}/>85 9.9999-9999</p> */}
                        </div>
                        <div className="coluna">
                            <h1>Aqui voçê conhece onde tudo começou</h1>
                            {/* <p className={styles.fone}><FaPhoneAlt className={styles.icon}/>85 9.9999-9999</p> */}
                        </div>
                    </div>
                    <div className="row">
                       <div className="coluna">
                            <p className={styles.end}> Av. Dom Luís, 812 - Aldeota, Fortaleza - CE, </p>
                       </div>
                       <div className="coluna">
                            <p className={styles.end}>Veja nossos lançamentos em primeira mão</p>
                       </div>
                       <div className="coluna">
                            <p className={styles.end}> Venha conhecer nossa história</p>
                       </div>
                    </div>
                    <div className="row">
                       <div className="coluna">
                       <a href="https://api.whatsapp.com/message/KKFC5REFXEFWH1" target="_blank" className={styles.whatsapp}><FaWhatsapp/></a>
                       </div>
                       <div className="coluna">
                       <a href="https://www.instagram.com/petersonforpets/" target="_blank" className={styles.instagram}><FaInstagram/></a>
                       </div>
                       <div className="coluna">
                       <a href="https://pt-br.facebook.com/centroesteticopetronypeterson" target="_blank" className={styles.instagram}><FaFacebookF/></a>
                       </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Contato;
import styles from "./BannerProduto.module.scss";
import Slider from "react-slick";

function BannerProduto(props) {

    const Right = props => (
        <div>
            {/* <img src="/assets/img/right.png" onClick={props.onClick} className='slick-next'/> */}
            <FiArrowRightCircle onClick={props.onClick} className={styles.slick_next} />
        </div>
    )

    const Left = props => (
        <div>
            {/* <img src="/assets/img/left.png" onClick={props.onClick} className='slick-prev'/> */}
            <FiArrowLeftCircle onClick={props.onClick} className={styles.slick_prev} />
        </div>
    )

    const banners = {
        dots: false,
        infinite: true,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        // nextArrow: <Right />,
        // prevArrow: <Left />,
    };


    return (
        <>
            <section className={styles.banner_servicos}>
                <Slider {...banners}>
                    <div>
                        <div className={styles.slider} style={{ 'backgroundImage': `url('/assets/img/banner-teste.jpg')` }}></div>
                    </div>
                    <div>
                        <div className={styles.slider} style={{ 'backgroundImage': `url('/assets/img/banner-teste.jpg')` }}></div>
                    </div>
                    <div>
                        <div className={styles.slider} style={{ 'backgroundImage': `url('/assets/img/banner-teste.jpg')` }}></div>
                    </div>
                    <div>
                        <div className={styles.slider} style={{ 'backgroundImage': `url('/assets/img/banner-teste.jpg')` }}></div>
                    </div>
                </Slider>
            </section>
        </>
    )
}

export default BannerProduto;
import styles from "./Banner.module.scss";
import { FaInstagram,FaWhatsapp, FaFacebookF, FaBarcode, FaBsEnvelope, FaPlayCircle} from 'react-icons/fa';
import { BsEnvelope } from "react-icons/bs";
import Slider from "react-slick";
import { FiArrowRightCircle, FiArrowLeftCircle } from "react-icons/fi";

function Banner(props){

    const Right = props => (
        <div>
          {/* <img src="/assets/img/right.png" onClick={props.onClick} className='slick-next'/> */}
          <FiArrowRightCircle onClick={props.onClick} className={styles.slick_next}/>
        </div>
      )
    
      const Left = props => (
        <div>
          {/* <img src="/assets/img/left.png" onClick={props.onClick} className='slick-prev'/> */}
          <FiArrowLeftCircle onClick={props.onClick} className={styles.slick_prev}/>
        </div>
      )
    
      const banners = {
        dots: false,
        infinite: true,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <Right />,
        prevArrow: <Left />,
      };


    return (
        <>
        <section className={styles.banner}>
        <Slider {...banners}>
        <div>
            <div className={styles.slider} style={{ 'backgroundImage': `url('/assets/img/banner.jpg')` }}>
                <div className="container">
                    <div className="row">
                        <div className="coluna">
                            <h1>Solução para melhorar o resultado da sua operação</h1>
                        </div>
                        {/* <div className={styles.coluna_play}>
                            <FaPlayCircle className={styles.icon_play} />
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div className={styles.slider} style={{ 'backgroundImage': `url('/assets/img/banner.jpg')` }}>
                <div className="container">
                    <div className="row">
                        <div className="coluna">
                            <h1>Solução em peças para a linha leve e pesada</h1>
                        </div>
                        {/* <div className={styles.coluna_play}>
                            <FaPlayCircle className={styles.icon_play} />
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
      </Slider>
        </section>
        </>
    )
}

export default Banner;
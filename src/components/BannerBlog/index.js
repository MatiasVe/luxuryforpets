import styles from "./BannerBlog.module.scss";

function BannerBlog(props){


    return (
        <>
        <section className={styles.banner_blog}>
            <div className="container">
                <div className="row">
                    <div className="coluna">
                        <h1>Blog</h1> 
                    </div>
                </div>
            </div>
        </section>
        </>
    )
}

export default BannerBlog;
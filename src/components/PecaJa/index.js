import styles from "./PecaJa.module.scss";

import { FaWhatsappSquare, FaInstagram, FaFacebookSquare } from "react-icons/fa";

function PecaJa(props) {


    return (
        <>
            <section className={styles.newslatter} style={{ 'backgroundImage': `url('/assets/img/img.png')` }}>
                <div className="container">
                    <div className="row">
                        <div className={styles.coluna}>
                            <h1>
                               Entre em contato
                               e peça já o seu 
                            </h1>
                        </div>
                        <div className={styles.coluna}>
                        <a href="https://api.whatsapp.com/message/KKFC5REFXEFWH1" target="_blank"><FaWhatsappSquare className={styles.what_icon} /></a>
                        <a href="https://www.instagram.com/petersonforpets/" target="_blank"><FaInstagram className={styles.insta_icon} /></a>
                        {/* <FaFacebookSquare className={styles.search_icon} /> */}
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default PecaJa;
import Header from "../components/Header";
import BannerEmpresa from "../components/BannerEmpresa";
import Sobre from "../components/Sobre";
import Aguarda from "../components/Aguarda";
import Footer from "../components/Footer";
import InfoRigth from "../components/InfoRigth";
import InfoLeft from "../components/InfoLeft";

function Index(props) {
  return (
    <>
      <Header/>
      <BannerEmpresa/>
      <Sobre/>
      <InfoRigth/>
      <InfoLeft/>
      <Aguarda/>
      <Footer/>
    </>
  )
}

export default Index;